function CartManager() {
    this.shoppingCart = [];
    this.budget;
    this.currentTotal = 0;
    this.displayFlag = false;

    CartManager.prototype.checkBudget = function() {
        if (this.budget != undefined) {
            if (this.currentTotal > this.budget) {
                document.getElementById('budget-limit-warning').innerHTML = 'YOU CANNOT AFFORD ALL ITEMS WITH THE CURRENT BUDGET.';
            } else {
                document.getElementById('budget-limit-warning').innerHTML = '';
            }
        }
    }

    CartManager.prototype.addItem = function(newItem) {
        this.shoppingCart.push(newItem);
        this.currentTotal += parseInt(newItem.totalPrice());
        this.checkBudget();
    }
}

CartManager.prototype.setBudget = function(budget) {
    this.budget = budget;
}

CartManager.prototype.removeItem = function(obj) {
    var id = obj.id;

    // Retrieve index of chosen item
    var index = this.shoppingCart.findIndex(e => e.id == obj.id); // ES6 syntax

    // remove from shoppingCart array
    var money = parseInt(obj.totalPrice());
    this.currentTotal = this.currentTotal - parseInt(obj.totalPrice());

    this.shoppingCart.splice(index, 1);

    // remove from HTML
    var element = document.getElementById(id);
    element.remove();

    // Update text
    this.checkBudget();
}