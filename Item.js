function Item(type, quantity, price, id) {
    this.type = type;
    this.quantity = quantity;
    this.price = price;
    this.id = id;

    Item.prototype.totalPrice = function() {
        return this.price * this.quantity;
    }
}