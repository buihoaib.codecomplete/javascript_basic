var myCart = new CartManager();
var counter = 0;

function addItem() {
    var type = document.getElementById('type').value;
    var quantity = document.getElementById('quantity').value;
    var price = document.getElementById('price').value;
    var id = counter++;

    // error checking
    if (type == '' || quantity == '' || price == '') {
        alert('Field must not be empty');
        return;
    }

    // Add new item to the shopping cart
    var newItem = new Item(type, quantity, price, id);
    myCart.addItem(newItem);
    updateTotalText();

    // Add new item to HTML
    addToHTML(newItem);

    // Clear input fields
    clearInputFields();
}

function addToHTML(newItem) {
    var newListItem = document.createElement('li');
    newListItem.innerHTML = newItem.type + ', ' + newItem.quantity + ', $' + newItem.totalPrice();
    newListItem.id = newItem.id;
    document.getElementById('shopping-cart').appendChild(newListItem);


    // add remove icon to new item
    var trashBin = document.createElement('i');
    trashBin.classList.add('fas');
    trashBin.classList.add('fa-trash');

    // Add onclick for item deletion 
    trashBin.onclick = function() {
        myCart.removeItem(newItem);
        updateTotalText();
    }

    var listLength = document.getElementsByTagName('li').length;
    document.getElementsByTagName('li')[listLength - 1].appendChild(trashBin);

}

function clearInputFields() {
    var list = document.getElementsByTagName('input');
    for (let i = 1; i < list.length; i++) {
        list[i].value = '';
    }
}

function setBudget() {
    var budget = document.getElementById('budget').value;

    // error checking
    if (budget == '') {
        alert('Field must not be empty');
        return;
    }

    myCart.setBudget(budget);

    document.getElementById('budget-text').innerHTML = '$' + budget;
}

function showCart() {
    var element = document.getElementById('shopping-cart');

    if (myCart.displayFlag === false) {
        element.style.display = 'none';
        myCart.displayFlag = true;
    } else {
        element.style.display = 'block';
        myCart.displayFlag = false;
    }
}

function updateTotalText() {
    document.getElementById('total-text').innerHTML = '$' + myCart.currentTotal;
}

function setNoBudget() {
    // CSS styling
    var button = document.getElementById('no-budget-button');
    button.style.color = 'grey';
    button.style.pointerEvents = 'none';
    button.style.border = '3pt solid black';

    // remove budget from CartManager object
    delete CartManager.budget;
}